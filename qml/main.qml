import QtQuick 2.0
import "engine"

Rectangle {
    width: 800
    height: 600

    FPSIndicator {

    }

    Screen {
        source: "Intro.qml"
    }

    MouseArea
    {
        anchors.fill: parent

        onClicked: {
            Qt.quit();
        }
    }
}
