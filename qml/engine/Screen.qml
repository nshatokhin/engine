import QtQuick 2.0

Rectangle {
    id: screen
    anchors.fill: parent

    property string source: ""
    property var component: null
    property var scene: null

    onSourceChanged: {
        if(source.length > 0) {
            component = Qt.createComponent(source);

            if (component.status === Component.Ready)
                    show();
                else
                    component.statusChanged.connect(show);
        }
    }

    function show() {
        if (component.status == Component.Ready) {
                scene = component.createObject(screen, {});
                if (scene == null) {
                    // Error Handling
                    console.log("Error creating object");
                }

            } else if (component.status === Component.Error) {
                // Error Handling
                console.log("Error loading component:", component.errorString());
            }
    }
}

