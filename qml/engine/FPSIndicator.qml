import QtQuick 2.0

Item {
    id: root
    visible: engine.showFps

    property real value: engine.fps

    Text {
        id: text
        text: root.value.toFixed(2).toString()
        color: "red"
        font.bold: true
    }
}

