#ifndef GAMESCENARIO_H
#define GAMESCENARIO_H

#include <QObject>

#include "configmanager.h"
#include "gamescenariostep.h"

class GameScenario : public QObject
{
    Q_OBJECT
public:
    explicit GameScenario(QObject *parent = 0);
    ~GameScenario();

    void load();
    GameScenarioStep *nextStep();

    void initialize();
    void destroy();

    void initScenario(const QString &scenario);
    
signals:
    
public slots:

protected:
    bool _initialized;

    //QMap <QString, GameScenarioStep *> _steps;
    QList<GameScenarioStep *> _steps;

    ConfigManager * _scenarioConfig;

    int _currentScenarioStep;

    void _reset();
    
};

#endif // GAMESCENARIO_H
