#ifndef MAINMANAGER_H
#define MAINMANAGER_H

#include <QObject>

#include "basicobject.h"
#include "configmanager.h"
#include "fileloadmanager.h"
#include "gamecontrollermanager.h"
#include "loadmanager.h"

class MainManager : public BasicObject
{
    Q_OBJECT
public:
    explicit MainManager(BasicObject *parent = 0);
    ~MainManager();

    static void init();
    static void clean();
    bool initialized();

    void processEvents();

    static ConfigManager * configManager();
    static LoadManager * loadManager();
    
signals:
    
public slots:

protected:
    static bool _initialized;

    static ConfigManager * _configManager;
    static FileLoadManager * _fileLoadManager;
    static GameControllerManager * _gameControllerManager;
    static LoadManager * _loadManager;

    static void _processEvents();

};

#endif // MAINMANAGER_H
