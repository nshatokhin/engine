#include "basicobject.h"

BasicObject::BasicObject(QObject *parent) : QObject(parent)
{
    _resetFlags();

    initialize();
}

BasicObject::~BasicObject()
{
    destroy();
}

void BasicObject::initialize()
{
    if(_initialized) return;

    _initialized = true;
}

void BasicObject::destroy()
{
    if(!_initialized) return;

    _initialized = false;
}

void BasicObject::_resetFlags()
{
    _initialized = false;
}

