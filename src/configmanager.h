#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QVariantList>

class ConfigManager : public QObject
{
    Q_OBJECT
public:
    explicit ConfigManager(QObject *parent = 0);
    ~ConfigManager();

    void initialize();
    void destroy();

    void load(const QString &configString);
    QString save();
    bool loadFromFile(const QString &filename);
    bool saveToFile(const QString &filename);
    
    int getIntValue(QString path, int defaultValue);
    void setIntValue(QString path, int value);
    bool getBoolValue(QString path, bool defaultValue);
    void setBoolValue(QString path, bool value);
    float getFloatValue(QString path, float defaultValue);
    void setFloatValue(QString path, float value);
    double getDoubleValue(QString path, double defaultValue);
    void setDoubleValue(QString path, double value);
    QString getStringValue(QString path, QString defaultValue);
    void setStringValue(QString path, QString value);
    QJsonObject getObjectValue(QString path, QJsonObject defaultValue);
    void setObjectValue(QString path, QJsonObject value);
    QVariantList getListValue(QString path, const QVariantList &defaultValue = QVariantList());
    void setListValue(QString path, QList<QVariant> value);
    QList<QVariantMap> getListOfObjectsValue(const QString &path, const QList<QVariantMap> &defaultValue = QList<QVariantMap>());
    void setListOfObjectsValue(const QString &path, const QList<QVariantMap> &value);

signals:
    
public slots:

protected:
    bool _initialized;

    QJsonDocument _json;
    QJsonObject _rootObject;

    void _resetFlags();

    QVariant _findValue(QString path, QVariant defaultValue);
    void _addValue(QString path, QVariant value);
    void _addRecursively(QJsonObject *json, QStringList path, QJsonValue value);
};

#endif // CONFIGMANAGER_H
