#include "loadworker.h"

#include <QFile>
#include <QTextStream>

LoadWorker::LoadWorker(QObject *parent) :
    QThread(parent)
{
    clear();
}

bool LoadWorker::isLoading()
{
    return _isLoading;
}

double LoadWorker::progress() const
{
    return _progress;
}

QMap<QString, QByteArray> LoadWorker::files() const
{
    return _files;
}

QByteArray LoadWorker::file(const QString &filepath)
{
    return _files.value(filepath);
}

QString LoadWorker::toString(QByteArray bytes)
{
    QTextStream stream(&bytes);
    return stream.readAll();
}

void LoadWorker::clear()
{
    _queue.clear();
    _files.clear();

    _isLoading = false;
}

void LoadWorker::startLoading()
{
    if(_isLoading) return;

    _oneFileWeight = 1.0 / _queue.count();
    _progress = 0;
    _isLoading = true;

    start();
}

void LoadWorker::stopLoading()
{
    _isLoading = false;

    exit();
}

void LoadWorker::addToQueue(const QString &filename)
{
    _queue.append(filename);
}


void LoadWorker::run()
{
    QFile file;
    QByteArray data;

    foreach (QString filename, _queue) {
        file.setFileName(filename);

        if(!file.open(QIODevice::ReadOnly)) {
            emit error(this, filename, file.errorString());

            _queue.removeAll(filename);

            continue;
        }

        data = file.readAll();

        _files.insert(filename, data);

        file.close();

        setProgress(progress() + _queue.removeAll(filename) * _oneFileWeight);

        emit fileLoaded(this, filename, data);
    }

    emit loadingFinished(this);

    stopLoading();
}

void LoadWorker::setProgress(const double &progress)
{
    if(_progress != progress) {
        _progress = progress;

        emit progressChanged(this, _progress);
    }
}

