#include "loadmanager.h"

LoadManager::LoadManager(QObject *parent) :
    QObject(parent)
{
}

LoadManager::~LoadManager()
{
    foreach (LoadWorker * worker, _workers) {
        destroyWorker(worker);
    }
}

LoadWorker *LoadManager::createWorker(QObject * receiver, const char * slot)
{
    LoadWorker * worker = new LoadWorker(this);
    connect(worker, SIGNAL(loadingFinished(LoadWorker *)), receiver, slot);

    _workers.insert(worker);

    return worker;
}

void LoadManager::destroyWorker(LoadWorker * worker)
{
    worker->exit();
    delete worker;
    _workers.remove(worker);
}
