#include "gamelevel.h"

GameLevel::GameLevel(QObject *parent) :
    QObject(parent)
{
}

GameLevel::~GameLevel()
{

}

void GameLevel::initialize()
{
    if(_initialized) return;

    _initialized = true;
}

void GameLevel::destroy()
{
    if(!_initialized) return;

    _initialized = false;
}

void GameLevel::load()
{
    if(_loaded)
        unload();

    _loaded = true;
}

void GameLevel::unload()
{

}

void GameLevel::processEvents()
{

}
