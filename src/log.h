#ifndef LOG_H
#define LOG_H

#include <QObject>
#include <QString>

#define CRITICAL_MSG QObject::tr("CRITICAL_ERROR:")
#define ERROR_MSG QObject::tr("ERROR:")
#define WARNING_MSG QObject::tr("WARNING:")
#define INFO_MSG QObject::tr("INFO:")

class Log
{
public:
    Log();
    ~Log();

    enum ErrorCode {SUCCESS = 0, ERROR, CRITICAL_ERROR, WARNING, INFO};

    static void critical(const QString &msg);
    static void error(const QString &msg);
    static void warning(const QString &msg);
    static void info(const QString &msg);
};

#endif // LOG_H
