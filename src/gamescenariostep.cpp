#include "gamescenariostep.h"

GameScenarioStep::GameScenarioStep(QObject *parent) :
    QObject(parent)
{
}

int GameScenarioStep::type()
{
    return _type;
}

void GameScenarioStep::setType(int type)
{
    if(_type != type)
    {
        _type = type;

        emit typeChanged(_type);
    }
}
