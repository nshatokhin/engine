#ifndef GAMESCENARIOSTEP_H
#define GAMESCENARIOSTEP_H

#include <QObject>

class GameScenarioStep : public QObject
{
    Q_OBJECT
public:
    enum Types {TYPE_SEQUENCE=0, TYPE_LEVEL};

    explicit GameScenarioStep(QObject *parent = 0);

    int type();
    void setType(int type);
    
signals:
    void typeChanged(int type);

public slots:

protected:
    int _type;
    
};

#endif // GAMESCENARIOSTEP_H
