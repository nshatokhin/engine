#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <QObject>

class StateMachine : public QObject
{
    Q_OBJECT
public:
    explicit StateMachine(QObject *parent = 0);
    ~StateMachine();

    void initialize();
    void destroy();

    int state();
    void setState(int state);
    
signals:
    void stateChanged(bool state);
    
public slots:

protected:
    bool _initialized;
    int _state;

    void _resetFlags();
    
};

#endif // STATEMACHINE_H
