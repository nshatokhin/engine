#ifndef GAME_H
#define GAME_H

#include <QObject>

#include "gamelevel.h"
#include "gamescenario.h"
#include "gamescenariostep.h"
#include "log.h"
#include "mainmanager.h"
#include "sequence.h"
#include "statemachine.h"

class Game : public QObject
{
    Q_OBJECT
public:
    enum States {STATE_IDLE=0, STATE_SEQUENCE, STATE_PAGE, STATE_LEVEL};

    explicit Game(QObject *parent = 0);
    ~Game();

    void initialize();
    void destroy();

    void processEvents();

    void initScenario(const QString &filename);
    
signals:
    
public slots:

protected:
    bool _initialized;

    GameLevel * _level;
    GameScenario * _scenario;
    GameScenarioStep * _scenarioStep;
    Sequence * _sequence;
    StateMachine * _stateMachine;

    QString _scenarioFileName;

    void _resetFlags();

    void _createLevel();
    void _destroyLevel();
    void _loadLevel();
    void _unloadLevel();
    
    void _createSequence();
    void _destroySequence();
    void _loadSequence();
    void _unloadSequence();

protected slots:
    void _stepFinished();
    void _scenarioFinished();

    void _scenarioFileLoaded(LoadWorker *worker);
};

#endif // GAME_H
