#ifndef LOADWORKER_H
#define LOADWORKER_H

#include <QDataStream>
#include <QMap>
#include <QThread>

class LoadWorker : public QThread
{
    Q_OBJECT
public:
    explicit LoadWorker(QObject *parent = 0);

    bool isLoading();
    double progress() const;
    QMap<QString, QByteArray> files() const;
    QByteArray file(const QString &filepath);

    static QString toString(QByteArray bytes);
    
signals:
    void loadingFinished(LoadWorker *);
    void fileLoaded(LoadWorker *, const QString &filename, const QByteArray &file);
    void error(LoadWorker *, const QString &filename, const QString &message);
    void progressChanged(LoadWorker *, const double &progress);
    
public slots:
    void clear();
    void startLoading();
    void stopLoading();
    void addToQueue(const QString &filename);

protected:
    void run();

    void setProgress(const double &progress);

    QList<QString> _queue;
    QMap<QString, QByteArray> _files;

    bool _isLoading;
    
    double _progress;
    double _oneFileWeight;
};

#endif // LOADWORKER_H
