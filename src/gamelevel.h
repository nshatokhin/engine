#ifndef GAMELEVEL_H
#define GAMELEVEL_H

#include <QObject>

class GameLevel : public QObject
{
    Q_OBJECT
public:
    explicit GameLevel(QObject *parent = 0);
    ~GameLevel();

    void initialize();
    void destroy();
    void load();
    void unload();
    
    void processEvents();

signals:
    
public slots:

protected:
    bool _initialized;
    bool _loaded;

    void _resetFlags();
    
};

#endif // GAMELEVEL_H
