#include "mainmanager.h"

#include <QDebug>

#include "configconsts.h"

MainManager::MainManager(BasicObject *parent) :
    BasicObject(parent)
{
}

MainManager::~MainManager()
{

}

void MainManager::init()
{
    if(_initialized) return;

    _fileLoadManager = new FileLoadManager();
    _gameControllerManager = new GameControllerManager();

    _initialized = true;
}

void MainManager::clean()
{
    if(!_initialized) return;

    delete _gameControllerManager;
    delete _fileLoadManager;

    if(_configManager != NULL) {
        delete _configManager;
        _configManager = NULL;
    }
    delete _configManager;

    if(_loadManager != NULL) {
        delete _loadManager;
        _loadManager = NULL;
    }

    MainManager::_initialized = false;
}

bool MainManager::initialized()
{
    return MainManager::_initialized;
}

void MainManager::processEvents()
{
    MainManager::_processEvents();
}

ConfigManager *MainManager::configManager()
{
    if(_configManager == NULL) {
        _configManager = new ConfigManager();
        _configManager->loadFromFile(ConfigConsts::defaultConfigFilename);
    }

    return _configManager;
}

LoadManager *MainManager::loadManager()
{
    if(_loadManager == NULL) {
        _loadManager = new LoadManager();
    }

    return _loadManager;
}

bool MainManager::_initialized = false;

ConfigManager * MainManager::_configManager = NULL;
FileLoadManager * MainManager::_fileLoadManager = NULL;
GameControllerManager * MainManager::_gameControllerManager = NULL;
LoadManager * MainManager::_loadManager = NULL;

void MainManager::_processEvents()
{
    if(!_initialized) return;

    _gameControllerManager->processEvents();
}
