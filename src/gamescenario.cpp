#include "gamescenario.h"

#include <QDebug>

GameScenario::GameScenario(QObject *parent) :
    QObject(parent)
{
    _reset();

    _scenarioConfig = new ConfigManager(this);

    _initialized = true;
}

GameScenario::~GameScenario()
{
    delete _scenarioConfig;
}

GameScenarioStep * GameScenario::nextStep()
{
    if(_steps.isEmpty() || _currentScenarioStep >= _steps.size()) return NULL;

    return _steps.at(_currentScenarioStep++);
}

void GameScenario::initScenario(const QString &scenario)
{
    _scenarioConfig->load(scenario);

    GameScenarioStep * step;

    QList<QVariant> stepsConfigs = _scenarioConfig->getListValue("steps");

    for(int i=0;i<stepsConfigs.count();i++) {
        QVariantMap map = stepsConfigs.at(i).toMap();

        qDebug() << map;
    }
}

void GameScenario::_reset()
{
    _initialized = false;
    _currentScenarioStep = 0;
}
