#ifndef GAMECONTROLLERMANAGER_H
#define GAMECONTROLLERMANAGER_H

#include <QList>
#include <QObject>

#include <SDL.h>

struct GameController
{
    SDL_Joystick * joyId;
    const char * name;
    int numAxes;
    int numTrackballs;
    int numHats;
    int numButtons;
};

class GameControllerManager : public QObject
{
    Q_OBJECT
public:
    explicit GameControllerManager(QObject *parent = 0);
    ~GameControllerManager();

    void initialize();
    void destroy();

    void processEvents();
    
signals:
    void buttonPressed(SDL_JoystickID joyId, Uint8 button);
    void buttonReleased(SDL_JoystickID joyId, Uint8 button);
    void axisMotion(SDL_JoystickID joyId, Uint8 axis, Sint16 value);
    void hatMotion(SDL_JoystickID joyId, Uint8 hat, Uint8 value);
    void ballMotion(SDL_JoystickID joyId, Uint8 ball, Sint16 xrel, Sint16 yrel);
    
public slots:

protected:
    bool _initialized;
    QList <GameController> _controllers;
    SDL_Event event;

    void _resetFlags();
    
};

#endif // GAMECONTROLLERMANAGER_H
