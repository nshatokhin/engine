#include "engine.h"

#include <QDebug>
#include <QQmlContext>
#include <QQmlEngine>

Engine::Engine(int argc, char *argv[], const char *mainView, BasicObject *parent) :
    BasicObject(parent)
{
    _resetFlags();

    initialize(argc, argv, mainView);
}

Engine::~Engine()
{
    destroy();
}

void Engine::initialize(int argc, char *argv[], const char *mainView)
{
    if(_initialized) return;

    _initialized = true;

    _initQt(argc, argv, mainView);

    _mainManager = new MainManager(this);
    MainManager::init();

    _initGame();
    _connectGame();
}

void Engine::destroy()
{
    if(!_initialized) return;

    _initialized = false;

    _disconnectGame();
    _destroyGame();

    MainManager::clean();
    delete _mainManager;

    _destroyQt();
}

int Engine::exec()
{
    initFrameRateLimitation();

    while(!_quit)
    {
        prepareFrameRateLimitation();
        calculateFPS();

        process();

        countFrames();
        limitFrameRate();
    }

    deinitFrameRateLimitation();

    return 0;
}

float Engine::fps() const
{
    return _avgFPS;
}

bool Engine::showFps() const
{
    return _mainManager->configManager()->getBoolValue(ConfigConsts::showFrameRateValuePath, ConfigConsts::defaultShowFrameRateValue);
}

void Engine::setShowFps(const bool &showFps)
{
    bool currentValue = _mainManager->configManager()->getBoolValue(ConfigConsts::showFrameRateValuePath, ConfigConsts::defaultShowFrameRateValue);

    if(currentValue != showFps) {
        _mainManager->configManager()->setBoolValue(ConfigConsts::showFrameRateValuePath, showFps);

        emit showFpsChanged(showFps);
    }
}

void Engine::process()
{
    _app->processEvents();

    _mainManager->processEvents();

    _game->processEvents();
}

void Engine::_resetFlags()
{
    BasicObject::_resetFlags();
    _quit = false;
}

void Engine::_initQt(int argc, char *argv[], const char *mainView)
{
    qDebug() << "Init Qt";

    _app = new QGuiApplication(argc, argv);
    _view = new QmlView();

    connect(_app, SIGNAL(lastWindowClosed()), this, SLOT(_applicationQuit()));

    _view->setResizeMode(QmlView::SizeRootObjectToView);
    connect(_view, SIGNAL(quit()), this, SLOT(_applicationQuit()));
    QQmlEngine::connect(_view->engine(), SIGNAL(quit()), this, SLOT(_applicationQuit()));
    _view->rootContext()->setContextProperty("engine", this);
    _view->setSource(QUrl(mainView));

    qDebug() << "Trying to show view...";

    _view->show();

    qDebug() << "View showed";
}

void Engine::_destroyQt()
{
    delete _view;
    delete _app;
}

void Engine::_initGame()
{
    _game = new Game();
    _game->initScenario(ConfigConsts::defaultMainScenarioFilename);
}

void Engine::_destroyGame()
{
    delete _game;
}

void Engine::_connectGame()
{

}

void Engine::_disconnectGame()
{

}

void Engine::initFrameRateLimitation()
{
    _screenTicksPerFrame = 1000 / _mainManager->configManager()->getIntValue(ConfigConsts::frameRatePath, ConfigConsts::defaultFrameRate);

    //Start counting frames per second
    _countedFrames = 0;
    _fpsTimer.start();
}

void Engine::deinitFrameRateLimitation()
{
    _fpsTimer.stop();
}

void Engine::prepareFrameRateLimitation()
{
    //Start cap timer
    _capTimer.start();
}

void Engine::limitFrameRate()
{
    //If frame finished early
    int frameTicks = _capTimer.getTicks();
    _capTimer.stop();

    if( frameTicks < _screenTicksPerFrame )
    {
        //Wait remaining time
        SDL_Delay( _screenTicksPerFrame - frameTicks );
    }
}

void Engine::countFrames()
{
    ++_countedFrames;
}

void Engine::calculateFPS()
{
    float fps = _countedFrames / ( _fpsTimer.getTicks() / 1000.f );

    if(fps != _avgFPS) {
        _avgFPS = fps;

        emit fpsChanged(_avgFPS);
    }

    //qDebug() << "FPS:" << _avgFPS;
}

void Engine::_applicationQuit()
{
    _quit = true;
}
