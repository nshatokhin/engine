#ifndef QMLVIEW_H
#define QMLVIEW_H

#include <QDebug>
#include <QQuickView>

class QmlView : public QQuickView
{
    Q_OBJECT

public:
    QmlView();

signals:
    void quit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseButtonPressed(int button);
    void mouseButtonReleased(int button);
    void mouseMoved(int x, int y);
    void mouseWheelWithPixels(QPoint pixels);
    void mouseWheelWithDegrees(QPoint degrees);

protected:
    bool event(QEvent *e) Q_DECL_OVERRIDE;
};

#endif // QMLVIEW_H
