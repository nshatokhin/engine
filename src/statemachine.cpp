#include "statemachine.h"

StateMachine::StateMachine(QObject *parent) :
    QObject(parent)
{
    _resetFlags();

    initialize();
}

StateMachine::~StateMachine()
{
    destroy();
}

void StateMachine::initialize()
{
    if(_initialized) return;

    _initialized = true;
}

void StateMachine::destroy()
{
    if(!_initialized) return;

    _initialized = false;
}

int StateMachine::state()
{
    return _state;
}

void StateMachine::setState(int state)
{
    if(_state != state)
    {
        _state = state;

        emit stateChanged(_state);
    }
}

void StateMachine::_resetFlags()
{
    _initialized = false;
    _state = 0;
}
