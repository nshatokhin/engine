#ifndef TIMER_H
#define TIMER_H

#include <SDL.h>
#include <SDL_timer.h>

#include "basicobject.h"

class Timer : public BasicObject
{
    Q_OBJECT
public:
    Timer(BasicObject *parent = 0);
    ~Timer();

    //The various clock actions
    void start();
    void stop();
    void pause();
    void unpause();

    //Gets the timer's time
    Uint32 getTicks();

    //Checks the status of the timer
    bool isStarted();
    bool isPaused();

protected:
    //The clock time when the timer started
    Uint32 _startTicks;

    //The ticks stored when the timer was paused
    Uint32 _pausedTicks;

    //The timer status
    bool _paused;
    bool _started;
};

#endif // TIMER_H
