#ifndef CONFIGCONSTS_H
#define CONFIGCONSTS_H

#include <QString>

namespace ConfigConsts
{
    const QString defaultConfigFilename = ":/config/default_config.json";

    const QString defaultMainScenarioFilename = ":/scenarios/main_scenario.json";

    const QString frameRatePath = "performance/frameRate";
    const int defaultFrameRate = 60;
    const QString showFrameRateValuePath = "debug/showFrameRateValue";
    const bool defaultShowFrameRateValue = true;
}

#endif // CONFIGCONSTS_H
