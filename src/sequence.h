#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <QObject>

class Sequence : public QObject
{
    Q_OBJECT
public:
    explicit Sequence(QObject *parent = 0);
    
    void processEvents();

signals:
    
public slots:
    
};

#endif // SEQUENCE_H
