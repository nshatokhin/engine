#ifndef ENGINE_H
#define ENGINE_H

#include <SDL.h>

#include <QGuiApplication>

#include "basicobject.h"
#include "configconsts.h"
#include "game.h"
#include "mainmanager.h"
#include "timer.h"
#include "qmlview.h"

class Engine : public BasicObject
{
    Q_OBJECT

    Q_PROPERTY(float fps READ fps NOTIFY fpsChanged)
    Q_PROPERTY(bool showFps READ showFps WRITE setShowFps NOTIFY showFpsChanged)
public:
    explicit Engine(int argc, char *argv[], const char * mainView, BasicObject *parent = 0);
    ~Engine();

    void initialize(int argc, char *argv[], const char * mainView);
    void destroy();
    
    int exec();

    float fps() const;
    bool showFps() const;
    void setShowFps(const bool &showFps);

signals:
    void fpsChanged(const float &fps);
    void showFpsChanged(const bool &showFps);

public slots:
    void process();

protected:

    bool _quit;

    QGuiApplication * _app;
    QmlView * _view;

    MainManager * _mainManager;

    Game * _game;

    int _screenTicksPerFrame;
    int _countedFrames;
    float _avgFPS;
    //The frames per second timer
    Timer _fpsTimer;
    //The frames per second cap timer
    Timer _capTimer;

    void _resetFlags();

    void _initQt(int argc, char *argv[], const char * mainView);
    void _destroyQt();

    void _initGame();
    void _destroyGame();
    void _connectGame();
    void _disconnectGame();

    void initFrameRateLimitation();
    void deinitFrameRateLimitation();
    void prepareFrameRateLimitation();
    void limitFrameRate();
    void countFrames();
    void calculateFPS();

protected slots:
    void _applicationQuit();
};

#endif // ENGINE_H
