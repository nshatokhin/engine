#include "gamecontrollermanager.h"

#include <QDebug>

GameControllerManager::GameControllerManager(QObject *parent) :
    QObject(parent)
{
    _resetFlags();

    initialize();
}

GameControllerManager::~GameControllerManager()
{
    destroy();
}

void GameControllerManager::initialize()
{
    if(_initialized) return;

    if ( SDL_InitSubSystem ( SDL_INIT_JOYSTICK ) < 0 )
    {
        qDebug() <<  "Unable to initialize Joystick: " << SDL_GetError();
        return;
    }

    int _numControllers = SDL_NumJoysticks ();

    for(int i=0;i<_numControllers;i++)
    {
        SDL_Joystick * joy = SDL_JoystickOpen ( i );

        if ( joy == NULL || !SDL_IsGameController( i )) continue;

        GameController controller;

        controller.joyId = joy;
        controller.name = SDL_JoystickNameForIndex( i );
        controller.numAxes = SDL_JoystickNumAxes ( joy );
        controller.numTrackballs = SDL_JoystickNumBalls ( joy );
        controller.numHats = SDL_JoystickNumHats ( joy );
        controller.numButtons = SDL_JoystickNumButtons ( joy );

        _controllers.append(controller);
    }

    SDL_JoystickEventState ( SDL_QUERY );

    _initialized = true;
}

void GameControllerManager::destroy()
{
    if(!_initialized) return;

    GameController controller;
    SDL_Joystick * joy;

    while(!_controllers.isEmpty())
    {
        controller = _controllers.takeLast();
        joy = controller.joyId;

        SDL_JoystickClose( joy );
    }

    SDL_QuitSubSystem( SDL_INIT_JOYSTICK );

    _initialized = false;
}

void GameControllerManager::processEvents()
{
    while( SDL_PollEvent( &event ) )
    {
        qDebug() << "GameControllerManager: incoming event";

        if( event.type == SDL_JOYBUTTONDOWN )
        {
            qDebug() << "Button down: " << event.jbutton.button;

            emit buttonPressed(event.jbutton.which, event.jbutton.button);
        }

        if( event.type == SDL_JOYBUTTONUP )
        {
            qDebug() << "Button up: " << event.jbutton.button;

            emit buttonReleased(event.jbutton.which, event.jbutton.button);
        }

        if(event.type == SDL_JOYAXISMOTION)
        {
            emit axisMotion(event.jaxis.which, event.jaxis.axis, event.jaxis.value);
        }

        if(event.type == SDL_JOYHATMOTION)
        {
            emit hatMotion(event.jhat.which, event.jhat.hat, event.jhat.value);
        }

        if(event.type == SDL_JOYBALLMOTION)
        {
            emit ballMotion(event.jball.which, event.jball.ball, event.jball.xrel, event.jball.yrel);
        }
    }
}

void GameControllerManager::_resetFlags()
{
    _initialized = false;
}
