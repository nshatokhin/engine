#include "configmanager.h"

#include <QDebug>

#include "log.h"

ConfigManager::ConfigManager(QObject *parent) :
    QObject(parent)
{
    _resetFlags();

    initialize();
}

ConfigManager::~ConfigManager()
{
    destroy();
}

void ConfigManager::initialize()
{
    if(_initialized) return;

    _initialized = true;
}

void ConfigManager::destroy()
{
    if(!_initialized) return;

    _initialized = false;
}

void ConfigManager::load(const QString &configString)
{
    QJsonParseError  parseError;

    _json = QJsonDocument::fromJson(QByteArray(configString.toUtf8()), &parseError);

    if (parseError.error != QJsonParseError::NoError) {
      qDebug() << "Failed to parse configuration: " << parseError.errorString();
    }

    if(_json.isObject())
    {
        _rootObject = _json.object();
    }
    else
    {
        qDebug() << "Only object json supported";
    }
}

QString ConfigManager::save()
{
    _json.setObject(_rootObject);

    return QString(_json.toJson());
}

bool ConfigManager::loadFromFile(const QString &filename)
{
    QFile configFile;
    configFile.setFileName(filename);

    if(!configFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        Log::error(QString(tr("Can't open config file: %1: %2")).arg(filename).arg(configFile.errorString()));

        return false;
    }

    QString fileContent = configFile.readAll();

    load(fileContent);

    configFile.close();

    return true;
}

bool ConfigManager::saveToFile(const QString &filename)
{
    QFile configFile;
    configFile.setFileName(filename);

    if(!configFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        Log::error(QString(tr("Can't open config file for write: %1: %2")).arg(filename).arg(configFile.errorString()));

        return false;
    }

    QTextStream out(&configFile);   // we will serialize the data into the file
    out << save();

    configFile.close();

    return true;
}

int ConfigManager::getIntValue(QString path, int defaultValue)
{
    return _findValue(path, defaultValue).toInt();
}

void ConfigManager::setIntValue(QString path, int value)
{
    _addValue(path, value);
}

bool ConfigManager::getBoolValue(QString path, bool defaultValue)
{
    return _findValue(path, defaultValue).toBool();
}

void ConfigManager::setBoolValue(QString path, bool value)
{
    _addValue(path, value);
}

float ConfigManager::getFloatValue(QString path, float defaultValue)
{
    return _findValue(path, defaultValue).toFloat();
}

void ConfigManager::setFloatValue(QString path, float value)
{
    _addValue(path, value);
}

double ConfigManager::getDoubleValue(QString path, double defaultValue)
{
    return _findValue(path, defaultValue).toDouble();
}

void ConfigManager::setDoubleValue(QString path, double value)
{
    _addValue(path, value);
}

QString ConfigManager::getStringValue(QString path, QString defaultValue)
{
    return _findValue(path, defaultValue).toString();
}

void ConfigManager::setStringValue(QString path, QString value)
{
    _addValue(path, value);
}

QJsonObject ConfigManager::getObjectValue(QString path, QJsonObject defaultValue)
{
    return _findValue(path, defaultValue).toJsonObject();
}

void ConfigManager::setObjectValue(QString path, QJsonObject value)
{
    _addValue(path, value);
}

QVariantList ConfigManager::getListValue(QString path, const QVariantList &defaultValue)
{
    return _findValue(path, defaultValue).toList();
}

void ConfigManager::setListValue(QString path, QList<QVariant> value)
{
    _addValue(path, value);
}

QList<QVariantMap> ConfigManager::getListOfObjectsValue(const QString &path, const QList<QVariantMap> &defaultValue)
{
    QVariantMap object;
    QList<QVariantMap> result;

    QVariantList list = getListValue(path);

    QVariantMap listElement;

    for(int i=0;i<list.count();i++) {
        listElement = list.at(i).toMap();

        result.append(listElement);
    }

    return result;
}

void ConfigManager::setListOfObjectsValue(const QString &path, const QList<QVariantMap> &value)
{

}

void ConfigManager::_resetFlags()
{
    _initialized = false;
}

QVariant ConfigManager::_findValue(QString path, QVariant defaultValue)
{
    QStringList splitedPath = path.split('/');

    QJsonObject json = _rootObject;
    QString key;

    while(!splitedPath.isEmpty())
    {
        key = splitedPath.takeFirst();

        if(!json.contains(key))
            return defaultValue;

        if(!splitedPath.isEmpty())
            json = json.value(key).toObject();
    }

    return json.value(key).toVariant();
}

void ConfigManager::_addValue(QString path, QVariant value)
{
    QStringList splitedPath = path.trimmed().split('/');

    _addRecursively(&_rootObject, splitedPath, QJsonValue::fromVariant(value));
}

void ConfigManager::_addRecursively(QJsonObject * json, QStringList path, QJsonValue value)
{
    QString key;

    if(path.isEmpty()) return;

    key = path.takeFirst();
    if(!json->contains(key) || !json->value(key).isObject())
    {
        if(path.isEmpty())
        {
            json->insert(key, value);
            return;
        }
        else
            json->insert(key, QJsonObject());
    }

    QJsonObject next_node = json->value(key).toObject();

    _addRecursively(&next_node, path, value);

    json->remove(key);
    json->insert(key, next_node);
}
