#include "game.h"

// 1. Load scenario

Game::Game(QObject *parent) :
    QObject(parent)
{
    _resetFlags();

    initialize();
}

Game::~Game()
{
    destroy();
}

void Game::initialize()
{
    if(_initialized) return;

    _scenario = new GameScenario(this);
    _stateMachine = new StateMachine(this);

    _initialized = true;
}

void Game::destroy()
{
    if(!_initialized) return;

    delete _scenario;
    delete _stateMachine;

    _initialized = false;
}

void Game::processEvents()
{
    //Log::info("Game::processEvents()");

    int state = _stateMachine->state();
//Log::info(QString("%1").arg(state));
    if(state == Game::STATE_IDLE)
    {
        _scenarioStep = _scenario->nextStep();

        if(_scenarioStep)
        {
            int type = _scenarioStep->type();

            if(type == GameScenarioStep::TYPE_SEQUENCE)
            {
                _createSequence();
            }
            else if(type == GameScenarioStep::TYPE_LEVEL)
            {
                _createLevel();
            }
        }
        else
            _scenarioFinished();
    }
    else if(state == Game::STATE_SEQUENCE)
    {
        _sequence->processEvents();
    }
    else if(state == Game::STATE_LEVEL)
    {
        _level->processEvents();
    }
}

void Game::initScenario(const QString &filename)
{
    _scenarioFileName = filename;

    LoadWorker * worker = MainManager::loadManager()->createWorker(this, SLOT(_scenarioFileLoaded(LoadWorker *)));
    worker->addToQueue(_scenarioFileName);
    worker->startLoading();
}

void Game::_resetFlags()
{
    _initialized = false;
}

void Game::_createLevel()
{
    Log::info("Game::_createLevel()");

    _level = new GameLevel(this);

    _stateMachine->setState(Game::STATE_LEVEL);
}

void Game::_destroyLevel()
{
    Log::info("Game::_destroyLevel()");

    delete _level;
}

void Game::_loadLevel()
{
    _level->load();
}

void Game::_unloadLevel()
{
    _level->unload();
}

void Game::_createSequence()
{
    Log::info("Game::_createSequence()");

    _sequence = new Sequence(this);

    _stateMachine->setState(Game::STATE_SEQUENCE);
}

void Game::_destroySequence()
{
    Log::info("Game::_destroySequence()");

    delete _sequence;
}

void Game::_loadSequence()
{

}

void Game::_unloadSequence()
{

}

void Game::_stepFinished()
{
    Log::info("Game::_stepFinished()");

    int type = _scenarioStep->type();

    if(type == GameScenarioStep::TYPE_SEQUENCE)
    {
        _destroySequence();
    }
    else if(type == GameScenarioStep::TYPE_LEVEL)
    {
        _destroyLevel();
    }

    _stateMachine->setState(Game::STATE_IDLE);
}

void Game::_scenarioFinished()
{
    //Log::info("Game::_scenarioFinished()");
}

void Game::_scenarioFileLoaded(LoadWorker * worker)
{
    QByteArray file = worker->file(_scenarioFileName);
    MainManager::loadManager()->destroyWorker(worker);

    _scenario->initScenario(LoadWorker::toString(file));
}
