#ifndef BASICOBJECT_H
#define BASICOBJECT_H

#include <QObject>

class BasicObject : public QObject
{
    Q_OBJECT
public:
    explicit BasicObject(QObject *parent = 0);
    ~BasicObject();

    virtual void initialize();
    virtual void destroy();

signals:

public slots:

protected:
    bool _initialized;

    virtual void _resetFlags();
};

#endif // BASICOBJECT_H
