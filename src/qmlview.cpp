#include "qmlview.h"

QmlView::QmlView()
{
}

bool QmlView::event(QEvent *e)
{
    int type = e->type();

    if (type == QEvent::Close)
        emit quit();

    if(type == QEvent::KeyPress)
        emit keyPressed(((QKeyEvent *) e)->key());

    if(type == QEvent::KeyRelease)
        emit keyReleased(((QKeyEvent *) e)->key());

    if(type == QEvent::MouseButtonPress)
        emit mouseButtonPressed(((QMouseEvent *) e)->button());

    if(type == QEvent::MouseButtonRelease)
        emit mouseButtonReleased(((QMouseEvent *) e)->button());

    if(type == QEvent::MouseMove)
        emit mouseMoved(((QMouseEvent *) e)->x(), ((QMouseEvent *) e)->y());

    if(type == QEvent::Wheel)
    {
        QPoint numPixels = ((QWheelEvent *) e)->pixelDelta();
        QPoint numDegrees = ((QWheelEvent *) e)->angleDelta() / 8;

        if (!numPixels.isNull()) {
            emit mouseWheelWithPixels(numPixels);
        } else if (!numDegrees.isNull()) {
            QPoint numSteps = numDegrees / 15;
            emit mouseWheelWithDegrees(numSteps);
        }
    }

    return QWindow::event(e);
}
