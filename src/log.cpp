#include "log.h"

#include <QDebug>

Log::Log()
{

}

Log::~Log()
{

}

void Log::critical(const QString &msg)
{
    qDebug() << CRITICAL_MSG << msg;

    exit(CRITICAL_ERROR);
}

void Log::error(const QString &msg)
{
    qDebug() << ERROR_MSG << msg;
}

void Log::warning(const QString &msg)
{
    qDebug() << WARNING_MSG << msg;
}

void Log::info(const QString &msg)
{
    qDebug() << INFO_MSG << msg;
}
