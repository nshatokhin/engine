#ifndef LOADMANAGER_H
#define LOADMANAGER_H

#include <QObject>
#include <QSet>

#include "loadworker.h"

class LoadManager : public QObject
{
    Q_OBJECT
public:
    explicit LoadManager(QObject *parent = 0);
    ~LoadManager();
    
    LoadWorker * createWorker(QObject *receiver, const char *slot);
    void destroyWorker(LoadWorker *worker);

signals:
    
public slots:

protected:
    QSet<LoadWorker *> _workers;
    
};

#endif // LOADMANAGER_H
