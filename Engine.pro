#-------------------------------------------------
#
# Project created by QtCreator 2013-11-22T03:13:02
#
#-------------------------------------------------

QT       += core quick gui

TARGET = Engine
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    src/engine.cpp \
    src/qmlview.cpp \
    src/gamecontrollermanager.cpp \
    src/game.cpp \
    src/gamelevel.cpp \
    src/statemachine.cpp \
    src/sequence.cpp \
    src/gamescenario.cpp \
    src/levelscenario.cpp \
    src/gamescenariostep.cpp \
    src/mainmanager.cpp \
    src/configmanager.cpp \
    src/fileloadmanager.cpp \
    src/loadmanager.cpp \
    src/loadworker.cpp \
    src/basicobject.cpp \
    src/timer.cpp \
    src/log.cpp \
    src/screen.cpp

HEADERS += \
    src/engine.h \
    src/qmlview.h \
    src/gamecontrollermanager.h \
    src/game.h \
    src/gamelevel.h \
    src/statemachine.h \
    src/sequence.h \
    src/gamescenario.h \
    src/levelscenario.h \
    src/gamescenariostep.h \
    src/mainmanager.h \
    src/configmanager.h \
    src/confignames.h \
    src/configconsts.h \
    src/fileloadmanager.h \
    src/loadmanager.h \
    src/loadworker.h \
    src/basicobject.h \
    src/timer.h \
    src/log.h \
    src/screen.h

INCLUDEPATH += src/ \
    /usr/include/SDL2/

OTHER_FILES += \
    qml/main.qml \
    config/default_config.json

RESOURCES += \
    resources.qrc

LIBS += -lSDL2

DISTFILES += \
    qml/engine/FPSIndicator.qml \
    scenarios/main_scenario.json \
    qml/engine/Screen.qml \
    qml/Intro.qml
